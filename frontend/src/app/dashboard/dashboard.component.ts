import { Component, OnInit } from '@angular/core';
import { Player } from '../player/player.model'
import { PlayerService } from '../player/player.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private players: Player[] = [];

  constructor(private playerService: PlayerService) {
  }

  ngOnInit() {
    this.playerService.getRanking()
      .subscribe(players => {
        this.players = players;
      });
  }
}

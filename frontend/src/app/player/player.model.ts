export class Player {
  key: string;
  name: string;
  score: number;
  rank: number;
}

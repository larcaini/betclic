import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Player } from './player.model';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) {
  }

  create(name: string): Observable<Player> {
    return this.http.post<Player>('http://localhost:8080/player/create?' + this.encodeQueryData({ 'name': name }), {});
  }

  updateScore(name: string, score: number): Observable<Player> {
    return this.http.put<Player>('http://localhost:8080/player/score?' + this.encodeQueryData({ 'name': name, 'score': score }), {});
  }

  getRanking(): Observable<Player[]> {
    return this.http.get<Player[]>('http://localhost:8080/player/ranking');
  }

  endTournament(): Observable<void> {
    return this.http.delete<void>('http://localhost:8080/player/end-tournament');
  }

  encodeQueryData(data): string {
    const result = [];
    for (let key in data) {
      result.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    }

    return result.join('&');
  }
}

package fr.larcaini.infrastructure.database

import com.mongodb.MongoClient
import io.dropwizard.lifecycle.Managed

class MongoManaged(var client: MongoClient) : Managed {

    override fun start() {
    }

    override fun stop() {
        client.close()
    }

}
package fr.larcaini.infrastructure.database

import com.mongodb.MongoClient
import com.mongodb.client.MongoDatabase
import fr.larcaini.infrastructure.config.AppConfiguration
import fr.larcaini.infrastructure.config.MongoConfiguration
import org.litote.kmongo.KMongo
import javax.inject.Inject

class MongoConnectionFactory {

    var mongoConfiguration: MongoConfiguration

    @Inject
    constructor(appConfiguration: AppConfiguration) {
        this.mongoConfiguration = appConfiguration.getMongoConfiguration()
    }

    fun client(): MongoClient {
        return KMongo.createClient(mongoConfiguration.host, mongoConfiguration.port)
    }

    fun db(): MongoDatabase {
        return client().getDatabase(mongoConfiguration.database)
    }
}
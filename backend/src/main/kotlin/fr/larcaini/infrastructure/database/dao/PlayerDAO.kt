package fr.larcaini.infrastructure.database.dao

import com.mongodb.client.MongoCollection
import fr.larcaini.application.player.Player
import fr.larcaini.infrastructure.database.MongoConnectionFactory
import org.litote.kmongo.*
import javax.inject.Inject

class PlayerDAO {

    private var mongoConnectionFactory : MongoConnectionFactory
    private var collection : MongoCollection<Player>

    @Inject
    constructor(mongoConnectionFactory: MongoConnectionFactory) {
        this.mongoConnectionFactory = mongoConnectionFactory
        this.collection = mongoConnectionFactory.db().getCollection()
    }

    fun findOne(name: String): Player? {
        return collection.findOne(Player::name eq name)
    }

    fun save(player: Player): Player {
        collection.save(player)

        return findOne(player.name) ?: player
    }

    fun saveAll(players: Collection<Player>) {
        players.forEach { player: Player -> save(player) }
    }

    fun findAll(): List<Player> {
        return collection.find().descendingSort(Player::score).toMutableList()
    }

    fun deleteAll() {
        collection.deleteMany()
    }
}
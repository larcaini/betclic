package fr.larcaini.infrastructure

import java.lang.RuntimeException

class NotFoundException : RuntimeException()
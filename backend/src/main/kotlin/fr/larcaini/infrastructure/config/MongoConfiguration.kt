package fr.larcaini.infrastructure.config

import io.dropwizard.Configuration

class MongoConfiguration(var host: String = "localhost", var port: Int = 27017, var database: String = "playerdb")

package fr.larcaini.infrastructure.config

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration

class AppConfiguration : Configuration() {

    private val name: String? = null
    private val dev: Boolean = true
    private var mongoConfiguration: MongoConfiguration? = null

    @JsonProperty
    fun getName(): String? {
        return name
    }

    @JsonProperty
    fun isDev(): Boolean {
        return dev
    }

    fun setMongoConfiguration(mongoConfiguration: MongoConfiguration) {
        this.mongoConfiguration = mongoConfiguration
    }

    @JsonProperty
    fun getMongoConfiguration(): MongoConfiguration {
        return mongoConfiguration ?: throw IllegalStateException()
    }
}

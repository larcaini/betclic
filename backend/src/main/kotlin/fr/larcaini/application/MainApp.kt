package fr.larcaini.application

import fr.larcaini.application.player.DaggerPlayerComponent
import fr.larcaini.application.player.Player
import fr.larcaini.application.player.PlayerModule
import fr.larcaini.infrastructure.AllowAllCORSFilter
import fr.larcaini.infrastructure.config.AppConfiguration
import fr.larcaini.infrastructure.database.MongoConnectionFactory
import fr.larcaini.infrastructure.database.MongoManaged
import fr.larcaini.infrastructure.database.dao.PlayerDAO
import io.dropwizard.Application
import io.dropwizard.setup.Environment

fun main(args: Array<String>) {
    MainApp().run(*args)
}

class MainApp : Application<AppConfiguration>() {

    override fun run(configuration: AppConfiguration, environment: Environment) {
        val mongoConnectionFactory = MongoConnectionFactory(configuration)
        val mongoManaged = MongoManaged(mongoConnectionFactory.client())

        val playerComponent = DaggerPlayerComponent.builder().playerModule(PlayerModule(configuration)).build()

        environment.lifecycle().manage(mongoManaged)
        environment.jersey().register(AllowAllCORSFilter())
        environment.jersey().register(playerComponent.maker())

        if (configuration.isDev()) {
            devInitialization(mongoConnectionFactory)
        }
    }

    private fun devInitialization(mongoConnectionFactory: MongoConnectionFactory) {
        val playerDAO = PlayerDAO(mongoConnectionFactory)

        playerDAO.deleteAll();
        addPlayer(playerDAO, "First", 10, 1)
        addPlayer(playerDAO, "Second", 7, 2)
        addPlayer(playerDAO, "Third", 5, 3)
        addPlayer(playerDAO, "Fourth", 3, 4)
    }

    private fun addPlayer(playerDAO: PlayerDAO, name: String, score: Int, rank: Int) {
        val player = Player(name)
        player.score = score
        player.rank = rank
        playerDAO.save(player)
    }
}
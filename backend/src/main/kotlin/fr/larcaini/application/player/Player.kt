package fr.larcaini.application.player

import org.bson.codecs.pojo.annotations.BsonId

data class Player(var name: String) {

    @BsonId
    val key: String? = null
    var score: Int = 0
    var rank: Int? = null

}

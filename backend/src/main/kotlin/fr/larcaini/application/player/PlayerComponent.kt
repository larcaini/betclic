package fr.larcaini.application.player

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(PlayerModule::class))
interface PlayerComponent {
    fun maker(): PlayerResource
}

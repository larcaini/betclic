package fr.larcaini.application.player

import fr.larcaini.infrastructure.NotFoundException
import javax.inject.Inject
import javax.ws.rs.*


@Path("/player")
@Produces("application/json")
class PlayerResource {

    var playerService: PlayerService

    @Inject
    constructor(playerService: PlayerService) {
        this.playerService = playerService;
    }

    @Path("/create")
    @POST
    fun create(@QueryParam("name") name: String): Player {
        return playerService.create(name)
    }

    @Path("/score")
    @PUT
    fun updateScore(@QueryParam("name") name: String, @QueryParam("score") score: Int): Player {
        return playerService.updateScore(name, score);
    }

    @Path("/{name}")
    @GET
    fun get(@PathParam("name") name: String): Player {
        return playerService.get(name) ?: throw NotFoundException();
    }

    @Path("/ranking")
    @GET
    fun ranking(): Collection<Player> {
        return playerService.ranking();
    }

    @Path("/end-tournament")
    @DELETE
    fun endTournament() {
        playerService.endTournament();
    }
}
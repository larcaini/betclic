package fr.larcaini.application.player

import fr.larcaini.infrastructure.AlreadyExistsException
import fr.larcaini.infrastructure.NotFoundException
import fr.larcaini.infrastructure.database.dao.PlayerDAO
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.Collectors

import javax.inject.Inject

class PlayerService {

    var playerDao: PlayerDAO

    @Inject
    constructor(playerDao: PlayerDAO) {
        this.playerDao = playerDao
    }

    fun create(name: String): Player {
        return playerDao.findOne(name)
            ?.also { throw AlreadyExistsException() }
            ?: let { return playerDao.save(Player(name)) }
    }

    fun updateScore(name: String, score: Int): Player {
        return playerDao.findOne(name)
            ?.let { player ->
                player.score = score
                playerDao.save(player)
                updateRanking()

                return get(name) ?: throw NotFoundException()
            }
            ?: run { throw NotFoundException() }
    }

    fun get(name: String): Player? {
        return playerDao.findOne(name)
    }

    fun ranking(): List<Player> {
        return playerDao.findAll()
    }

    fun endTournament() {
        playerDao.deleteAll()
    }

    internal fun updateRanking() {
        val rank = AtomicInteger(1)

        playerDao.saveAll(playerDao.findAll().stream()
            .peek { player -> player.rank = rank.getAndIncrement() }
            .collect(Collectors.toList()))
    }
}

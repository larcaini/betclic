package fr.larcaini.application.player

import dagger.Module
import dagger.Provides
import fr.larcaini.infrastructure.config.AppConfiguration
import javax.inject.Singleton

@Module
class PlayerModule {

    var appConfiguration: AppConfiguration

    constructor(appConfiguration: AppConfiguration) {
        this.appConfiguration = appConfiguration
    }

    @Provides
    @Singleton
    fun provideAppConfiguration() = appConfiguration
}
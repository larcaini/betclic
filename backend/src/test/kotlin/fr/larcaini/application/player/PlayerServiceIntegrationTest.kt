package fr.larcaini.application.player

import de.flapdoodle.embed.mongo.MongodExecutable
import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.runtime.Network.localhostIsIPv6
import fr.larcaini.infrastructure.AlreadyExistsException
import fr.larcaini.infrastructure.config.AppConfiguration
import fr.larcaini.infrastructure.config.MongoConfiguration
import fr.larcaini.infrastructure.database.MongoConnectionFactory
import fr.larcaini.infrastructure.database.dao.PlayerDAO
import org.junit.*


class PlayerServiceIntegrationTest {

    lateinit var mongodExecutable : MongodExecutable

    lateinit var playerService: PlayerService

    @Before
    fun setUp() {

        val bindIp = "localhost"
        val port = 27017
        val mongodConfig = MongodConfigBuilder()
            .version(Version.Main.PRODUCTION)
            .net(Net(bindIp, port, localhostIsIPv6()))
            .build()

        val starter = MongodStarter.getDefaultInstance()
        this.mongodExecutable = starter.prepare(mongodConfig)
        mongodExecutable.start()

        val mongoConfiguration = MongoConfiguration()
        mongoConfiguration.database = "playerdb"
        mongoConfiguration.host = bindIp
        mongoConfiguration.port = port

        val appConfiguration = AppConfiguration()
        appConfiguration.setMongoConfiguration(mongoConfiguration)

        val mongoConnectionFactory = MongoConnectionFactory(appConfiguration)
        val playerDAO = PlayerDAO(mongoConnectionFactory)

        playerService = PlayerService(playerDAO)
    }

    @After
    fun setDown() {
        mongodExecutable.stop()
    }

    @Test
    fun userCreated() {
        // Given

        val name = "larcaini"

        // When

        val newPlayer = playerService.create(name)

        // Then

        Assert.assertEquals("larcaini", newPlayer.name)
        Assert.assertEquals(0, newPlayer.score)
        Assert.assertNull(newPlayer.rank)
    }

    @Test(expected = AlreadyExistsException::class)
    fun userAlreadyExists() {
        // Given

        val name = "larcaini"
        playerService.create(name)

        // When

        playerService.create(name)

        // Then

        Assert.fail()
    }

    @Test
    fun userUpdateScore() {
        // Given

        val name = "larcaini"
        val score = 10
        playerService.create(name)

        // When

        val updatedPlayer = playerService.updateScore(name, score)

        // Then

        Assert.assertEquals("larcaini", updatedPlayer.name)
        Assert.assertEquals(10, updatedPlayer.score)
        Assert.assertEquals(1, updatedPlayer.rank)
    }

    @Test
    fun ranks() {
        // Given

        playerService.create("larcaini")
        playerService.updateScore("larcaini", 5)
        playerService.create("best")
        playerService.updateScore("best", 25)
        playerService.create("worst")
        playerService.updateScore("worst", 2)
        playerService.create("better")
        playerService.updateScore("better", 15)

        // When

        val ranking: List<Player> = playerService.ranking()

        // Then

        Assert.assertEquals(4, ranking.size)

        Assert.assertEquals("best", ranking.get(0).name)
        Assert.assertEquals(1, ranking.get(0).rank)
        Assert.assertEquals("better", ranking.get(1).name)
        Assert.assertEquals(2, ranking.get(1).rank)
        Assert.assertEquals("larcaini", ranking.get(2).name)
        Assert.assertEquals(3, ranking.get(2).rank)
        Assert.assertEquals("worst", ranking.get(3).name)
        Assert.assertEquals(4, ranking.get(3).rank)
    }
}
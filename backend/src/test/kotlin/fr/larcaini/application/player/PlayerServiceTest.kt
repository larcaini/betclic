package fr.larcaini.application.player

import fr.larcaini.infrastructure.AlreadyExistsException
import fr.larcaini.infrastructure.database.dao.PlayerDAO
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PlayerServiceTest {

    lateinit var playerService: PlayerService

    @MockK
    lateinit var playerDAO: PlayerDAO

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        every{ playerDAO.findOne("larcaini") } returns Player("larcaini")
        every{ playerDAO.findOne("John Smith") } returns null
        every{ playerDAO.save(any()) } returns Player("")

        playerService = PlayerService(playerDAO)
    }

    @Test(expected = AlreadyExistsException::class)
    fun userExists() {
        // Given

        val name = "larcaini"

        // When

        playerService.create(name)

        // Then

        Assert.fail()
    }

    @Test
    fun userCreated() {
        // Given

        val name = "John Smith"

        // When

        playerService.create(name)

        // Then

        verify { playerDAO.save(Player("John Smith")) }
    }
}
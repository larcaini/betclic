import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "fr.larcaini"
version = "1.0-SNAPSHOT"

plugins {
    application
    kotlin("jvm") version "1.3.31"
    kotlin("kapt") version "1.3.31"
}

application {
    mainClassName = "fr.larcaini.application.MainAppKt"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    compile("com.google.dagger:dagger:2.13")
    compile("io.dropwizard:dropwizard-core:1.3.15")
    compile("org.litote.kmongo:kmongo:3.11.1")
    kapt("com.google.dagger:dagger-compiler:2.13")

    testCompile("junit:junit:4.12")
    testCompile("de.flapdoodle.embed:de.flapdoodle.embed.mongo:2.2.0")
    testImplementation("io.mockk:mockk:1.9.3")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

kapt {
    arguments {
        arg("dagger.formatGeneratedSource", "disabled")
        arg("dagger.gradle.incremental", "enabled")
    }
}

val run by tasks.getting(JavaExec::class) {
    args("server", "config/local.yaml")
}
